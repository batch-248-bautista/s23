 let trainer = {
 	name : "Ash Ketchum",
 	age : 10,
 	pokemon: ["Pikachu", "Chrizard", "Squirtle", "Bulbasaur"],
 	friends: {
 		Hoenn: ["May", "Max"],
 		Kanto: ["Brock", "Misty"]

 	},
 	talk: function(){
			console.log(trainer.pokemon[0] + "! I choose you!");
		}
}
 
 console.log(trainer);
 
 console.log("Result of Dot Notation: ");
 console.log(trainer.name);
 console.log("Result of Square Bracket Notation: ");
 console.log( trainer["pokemon"]);



console.log("Result of talk method: ");
trainer.talk();
function Pokemon(name,level){

		this.name = name;
		this.level = level;
		this.health = level * 2;
		this.attack = level;
		this.tackle = function(target){
			console.log(this.name + " tackled " + target.name + ".");
				
			// 	//reduce the target object's health property by subtracting and reassining its value based on the pokemon's attack
			// 	//target.health = target.health - this.attack
				target.health -= this.attack

			// 	//example if health is not less than 0
			// 		//rattata's health is now reduced to 5

				console.log(target.name + " health is now reduced to " + target.health + ".");
				// console.log(target.name);
			// 	//if the target's health is less than or equal to 0 we will invoke the faint method, otherwise printout the pokemon's new health

				if(target.health<=0){
					target.faint()
				}

		}

		//create another method called faint
		this.faint = function(){
			console.log(this.name + " fainted.");
		}

	}

	let pikachu = new Pokemon ("Pikachu", 12);
	console.log(pikachu);
	let geodude = new Pokemon("Geodude", 8);
	console.log(geodude);
	let mewtwo = new Pokemon("Mewtwo", 100);
	console.log(mewtwo);

	geodude.tackle(pikachu);
	mewtwo.tackle(geodude);